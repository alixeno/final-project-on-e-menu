from django.shortcuts import render, redirect
from django.views.generic import *
from .models import *
from .forms import *

from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.urls import reverse_lazy, reverse



class PiHomeView(TemplateView):
	template_name = "pitemplates/pihome.html"

class PiMixin(object):
	def get_context_data(**kwargs):
		context = super().get_context_data(**kwargs)
		context["x"] = 5;
		context["y"] = 7;
		context["z"] = int(x) + int(y);
		return context

class PiProgram1View(PiMixin, TemplateView):
	template_name = 'pitemplates/piprogram1.html'