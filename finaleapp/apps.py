from django.apps import AppConfig


class FinaleappConfig(AppConfig):
    name = 'finaleapp'
