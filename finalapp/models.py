from django.db import models
from django.contrib.auth.models import User, Group
from .constants import *



class TimeStamp(models.Model):
	created_at = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now = True, null = True, blank = True)
	active = models.BooleanField(default = True)

	class Meta: 
		abstract = True



class Restaurant(TimeStamp):
	name = models.CharField(max_length = 300)
	email = models.EmailField()
	phone = models.CharField(max_length = 50)
	address = models.CharField(max_length = 200)
	about = models.TextField()
	profile_image = models.ImageField(upload_to = "restro")
	website = models.CharField(max_length = 100)
	map_location = models.CharField(max_length = 500)
	estb_date = models.DateField()

	def __str__(self):
		return self.name



class Admin(TimeStamp):
	user = models.OneToOneField(User, on_delete = models.CASCADE)
	name = models.CharField(max_length = 300)
	photo = models.ImageField(upload_to = 'user')
	mobile = models.CharField(max_length = 50)
	email = models.EmailField(null=True, blank=True)
	address = models.CharField(max_length = 150)
	
	def save(self, *args, **kwargs):
		grp, created = Group.objects.get_or_create(name ="admin")
		self.user.groups.add(grp)
		super().save(*args, **kwargs)

	def __str__(self):
		return self.name




class Table(TimeStamp):
	user = models.OneToOneField(User, on_delete = models.CASCADE)
	name = models.CharField(max_length = 300)

	def save(self, *args, **kwargs):
		grp, created = Group.objects.get_or_create(name ="table")
		self.user.groups.add(grp)
		super().save(*args, **kwargs)

	def __str__(self):
		return self.name



class Menu(TimeStamp):
	title = models.CharField(max_length = 100)
	root = models.ForeignKey('self', on_delete = models.SET_NULL, null = True, blank = True)

	def __str__(self):
		return self.title




class Item(TimeStamp):
	name = models.CharField(max_length = 300)
	menu = models.ForeignKey(Menu, on_delete = models.CASCADE)
	sku = models.CharField(max_length= 300, unique = True, null=True, blank=True)
	photo = models.ImageField(upload_to = "item")
	price = models.DecimalField(max_digits = 30,decimal_places=2)

	def __str__(self):
		return self.name




class Basket(TimeStamp):
	table = models.ForeignKey(Table, on_delete= models.CASCADE, null=True, blank=True)
	total = models.DecimalField(max_digits=30, decimal_places=2)

	def __str__(self):
		return "Basket id: " + str(self.id)



class BasketItem(TimeStamp):
	basket = models.ForeignKey(Basket, on_delete=models.CASCADE)
	item = models.ForeignKey(Item, on_delete=models.CASCADE)
	rate = models.DecimalField(max_digits=20, decimal_places=2)
	quantity = models.PositiveIntegerField()
	subtotal = models.DecimalField(max_digits=20, decimal_places=2)

	def __str__(self):
		return self.item.name + "(" + str(self.basket) + ")"




class Order(TimeStamp):
	basket = models.OneToOneField(Basket, on_delete=models.CASCADE)
	table = models.ForeignKey(Table, on_delete=models.CASCADE, null=True, blank=True)
	subtotal = models.DecimalField(max_digits=20, decimal_places=2)
	discount = models.DecimalField(max_digits=20, decimal_places=2)
	total = models.DecimalField(max_digits=20, decimal_places=2)
	order_status = models.CharField(max_length=50, choices=ORDER_STATUS)
	payment_method = models.CharField(max_length=50, choices = PAYMENT_METHOD)


	def __str__(self):
		return "Order id: " + str(self.id)


class Staff(TimeStamp):
	name = models.CharField(max_length = 300)
	address = models.CharField(max_length = 300)
	photo = models.ImageField(upload_to = "staff")
	mobile = models.CharField(max_length = 50, null=True, blank=True)
	position = models.CharField(max_length = 200)

	def __str__(self):
		return self.name