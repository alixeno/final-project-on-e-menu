from django.urls import path
from .views import *
# from .views import HomeView, ContactView

app_name="finalapp"

urlpatterns=[

	path("table/signup/", TableSignupView.as_view(), name = 'tablesignup'),
	path("table/login/", TableLoginView.as_view(), name = 'tablelogin'),
	path("signout/", SignoutView.as_view(), name = "signout"),


	path("home/", ClientHomeView.as_view(), name = 'clienthome'),

    path('menu/list/', ClientMenuListView.as_view(), name='clientmenulist'),

    path('', ClientItemListView.as_view(), name='clientitemlist'),

	path('menu/<int:pk>/detail/', ClientMenuDetailView.as_view(), name = 'clientmenudetail'),

	path("basket/", ClientBasketView.as_view(), name = 'clientbasket'),
    path("add-to-basket/<int:pk>/", AddToBasketView.as_view(), name = "addtobasket"),
    path("manage-basket/<int:pk>/<action>/", ManageBasketView.as_view(), name = "managebasket"),
    path("empty-basket/", EmptyBasketView.as_view(), name = 'emptybasket'),

    path("order/create/", OrderCreateView.as_view(), name = 'ordercreate'),
    path("order/list/", OrderListView.as_view(), name = 'orderlist'),
    path("order/<int:pk>/detail/", OrderDetailView.as_view(), name = 'orderdetail'),





    path("search/", ClientSearchView.as_view(), name = "clientsearch"),



	path("pi/", ClientPiHomeView.as_view(), name = 'clientpihome'),
    path("hardware/", HardwareView.as_view(), name = "clienthardware"),
    path("led/", LedView.as_view(), name = "clientled"),
    path("fan/", FanView.as_view(), name = "clientfan"),
    path("buzzer/", BuzzerView.as_view(), name = "clientbuzzer"),
    
#+++++++++++++++++++++++++++++
#+++++++++++++++++++++++++++++
#+++++++++++++++++++++++++++++
#+++++++++++++++++++++++++++++



    path("emenu/admin/signin/", AdminSigninView.as_view(), name = "adminsignin"),
    path("emenu/admin/signout/", AdminSignoutView.as_view(), name = "adminsignout"),
    
    path("emenu/admin/", AdminHomeView.as_view(), name = 'adminhome'),

    # path("emenu/admin/service/list/", AdminServiceListView.as_view(), name = 'adminservicelist'),
    # path("emenu/admin/service/<int:pk>/detail/", AdminServiceDetailView.as_view(),
    #     name = 'adminservicedetail'),
    # path("emenu/admin/service/create/", AdminServiceCreateView.as_view(),
    #     name = 'adminservicecreate'),
    # path("emenu/admin/service/<int:pk>/update/", AdminServiceUpdateView.as_view(),
    #     name = 'adminserviceupdate'),
    # path("emenu/admin/service/<int:pk>/delete/", AdminServiceDeleteView.as_view(),
    #     name = 'adminservicedelete'),

    path("emenu/admin/menu/list/", AdminMenuListView.as_view(), name = 'adminmenulist'),
    path("emenu/admin/menu/<int:pk>/detail/", AdminMenuDetailView.as_view(),
        name = 'adminmenudetail'),
    path("emenu/admin/menu/create/", AdminMenuCreateView.as_view(),
        name = 'adminmenucreate'),
    path("emenu/admin/menu/<int:pk>/update/", AdminMenuUpdateView.as_view(),
        name = 'adminmenuupdate'),
    path("emenu/admin/menu/<int:pk>/delete/", AdminMenuDeleteView.as_view(),
        name = 'adminmenudelete'),

    path("emenu/admin/item/list/", AdminItemListView.as_view(), name = 'adminitemlist'),
    path("emenu/admin/item/<int:pk>/detail/", AdminItemDetailView.as_view(),
        name = 'adminitemdetail'),
    path("emenu/admin/item/create/", AdminItemCreateView.as_view(),
        name = 'adminitemcreate'),
    path("emenu/admin/item/<int:pk>/update/", AdminItemUpdateView.as_view(),
        name = 'adminitemupdate'),
    path("emenu/admin/item/<int:pk>/delete/", AdminItemDeleteView.as_view(),
        name = 'adminitemdelete'),


    # path("emenu/admin/specialitem/list/", AdminSpecialItemListView.as_view(),
    #     name = 'adminspecialitemlist'),
    # path("emenu/admin/specialitem/<int:pk>/detail/", AdminSpecialItemDetailView.as_view(),
    #     name = 'adminspecialitemdetail'),
    # path("emenu/admin/specialitem/create/", AdminSpecialItemCreateView.as_view(),
    #     name = 'adminspecialitemcreate'),
    # path("emenu/admin/specialitem/<int:pk>/update/", AdminSpecialItemUpdateView.as_view(),
    #     name = 'adminspecialitemupdate'),
    # path("emenu/admin/specialitem/<int:pk>/delete/", AdminSpecialItemDeleteView.as_view(),
    #     name = 'adminspecialitemdelete'),


    path("emenu/admin/staff/list/", AdminStaffListView.as_view(), name = 'adminstafflist'),
    # path("emenu/admin/staff/signup/", AdminStaffSignupView.as_view(),
    #     name = 'adminstaffsignup'),
    path("emenu/admin/staff/create/", AdminStaffCreateView.as_view(), name = 'adminstaffcreate'),
    path("emenu/admin/staff/<int:pk>/detail/", AdminStaffDetailView.as_view(), name = 'adminstaffdetail'),
    path("emenu/admin/staff/<int:pk>/delete/", AdminStaffDeleteView.as_view(), name = 'adminstaffdelete'),
    # path("emenu/admin/staff/signin/", AdminStaffSigninView.as_view(),
    #     name = 'adminstaffsignin'),


    path("emenu/admin/basket/list/", AdminBasketListView.as_view(), name = 'adminbasketlist'),
    path("emenu/admin/order/list/", AdminOrderListView.as_view(), name = 'adminorderlist'),
    path("emenu/admin/order/<int:pk>/update/", AdminOrderUpdateView.as_view(),
        name = 'adminorderupdate'),
    path("emenu/admin/order/<int:pk>/detail/", AdminOrderDetailView.as_view(),
        name = 'adminorderdetail'),

    path("emenu/admin/search/", AdminSearchView.as_view(), name = "adminsearch"),


    path("emenu/kitchen/home/", KitchenHomeView.as_view(), name = "kitchenhome"),
    path("emenu/kitchen/order/list/", KitchenOrderListView.as_view(), name = "kitchenorderlist"),
    path("emenu/kitchen/order/<int:pk>/detail/", KitchenOrderDetailView.as_view(),
        name = "kitchenorderdetail"),

     path("emenu/try/", TryView.as_view(), name = "try"),


]