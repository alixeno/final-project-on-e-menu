

ORDER_STATUS = (
	("neworder", "New Order"),
	("preparedorder", "Prepared Order"),
	("deliveredorder", "Delivered Order"),
	)


PAYMENT_METHOD = (
	("cashpayment", "Cash Payment"),
	("cardswipe", "Card Swipe"),
	("epayment", "E-Payment"),
	)


