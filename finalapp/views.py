from django.shortcuts import render, redirect
from django.views.generic import *
from .models import *
from .forms import *

from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.urls import reverse_lazy, reverse


from .ledoff import ledoff
from .ledon import ledon
from .fanon import fanon
from .fanoff import fanoff
from .buzzeron import buzzeron
from .buzzeroff import buzzeroff




class HardwareView(View):
    def get(self, request, **kwargs):
        return render(request, "clienttemplates/clienthardware.html", {})
  

class LedView(View):
    def get(self, request, **kwargs):
        return render(request, "clienttemplates/clientled.html", {})
    
    def post(self, request, *args, **kwargs):
        led_sw = request.POST.get('led_switch')
       
        if led_sw == "ledon":
            ledon()
        else:
            ledoff()
        
        return render(request, "clienttemplates/clientled.html", {})
    
class FanView(View):
    def get(self, request, *args, **kwargs):
        return render(request, "clienttemplates/clientfan.html", {})
 
    def post(self, request, *args, **kwargs):
        fan_sw = request.POST.get('fan_switch')
        if fan_sw == "fanon":
            fanon()
        else:
            fanoff()
        return render(request, "clienttemplates/clientfan.html", {})
        


class BuzzerView(View):
    def get(self, request, *args, **kwargs):
        return render(request, "clienttemplates/buzzer.html", {})
 
    def post(self, request, *args, **kwargs):
        buzzer_sw = request.POST.get('buzzer_switch')
        if buzzer_sw == "buzzeron":
            buzzeron()
        else:
            buzzeroff()
        return render(request, "clienttemplates/buzzer.html", {})
        
    






# ClientMixin class

# Client ClientMixin View
class ClientMixin(object):
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context["restro"] = Restaurant.objects.all()
		context["allmenus"] = Menu.objects.filter(root=None)
		context["allitems"] = Item.objects.all().order_by("-id")
		context["allorders"] = Order.objects.all().order_by("-id")
		context["orderform"] = OrderForm
		context["signinform"] = SigninForm
		context["signupform"] = TableSignupForm


		# Assigning table to recently created or already present cart id
		basket_id = self.request.session.get("mybasket", None)
		logged_in_user = self.request.user
		if logged_in_user.is_authenticated and logged_in_user.groups.filter(name = "table").exists():
			table = Table.objects.get(user=logged_in_user)
		if basket_id:
			basket_obj = Basket.objects.get(id=basket_id)
			basket_obj.table = table
			basket_obj.save()


		return context




# Client TableRequiredMixin View
class TableRequiredMixin(object):
	def dispatch(self, request, *args, **kwargs):
		if request.user.is_authenticated and request.user.groups.filter(name="table").exists():
			pass
		else:
			return redirect("finalapp:tablelogin")

		return super().dispatch(request, *args, **kwargs)





# Client Table Signup View
class TableSignupView(ClientMixin, CreateView):
	template_name = 'clienttemplates/tablesignup.html'
	form_class = TableSignupForm
	success_url = reverse_lazy("finalapp:clienthome")
	def form_valid(self, form):
		uname = form.cleaned_data["username"]
		email = form.cleaned_data["email"]
		password = form.cleaned_data["password"]
		user = User.objects.create_user(uname, email, password)
		form.instance.user = user

		return super().form_valid(form)



# Table Login View Class
class TableLoginView(FormView):
	template_name = 'clienttemplates/tablelogin.html'
	form_class = SigninForm
	success_url = reverse_lazy("finalapp:clienthome")

	def form_valid(self, form):
		uname = form.cleaned_data['username']
		pwrd = form.cleaned_data['password']
		user = authenticate(username=uname, password=pwrd)
		if user is not None:
			login(self.request, user)
		else:
			return render(self.request, self.template_name,{
			'error': 'Invalid Username or Password!!!',
			'form': form
			})
		return super().form_valid(form)

	def get_success_url(self):
		logged_in_user = self.request.user
		if logged_in_user.groups.filter(name = 'table').exists():
			return reverse("finalapp:clienthome")
		else:
			return reverse("finalapp:tablesignup")




# Signout view class
class SignoutView(View):
	def get(self, request):
		logout(request)
		return redirect('/')




# Client Home View
class ClientHomeView(ClientMixin, TemplateView):
	template_name = "clienttemplates/clienthome.html"



class ClientMenuListView(ClientMixin, TemplateView):
	template_name = 'clienttemplates/clientmenulist.html'



class ClientItemListView(ClientMixin, TemplateView):
	template_name = 'clienttemplates/clientitemlist.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		menu = Item.objects.all()

		page = self.request.GET.get('page',1)

		paginator = Paginator(menu, 8)
		try:
			results = paginator.page(page)
		except PageNotAnInteger:
			results = paginator.page(1)
		except EmptyPage:
			results = paginator.page(paginator.num_pages)

		context["allitems"] = results

		return context



class ClientMenuDetailView(ClientMixin, TableRequiredMixin, DetailView):
	template_name = 'clienttemplates/clientmenudetail.html'
	model = Menu
	context_object_name = 'menuobject'




# Client Basket View
class ClientBasketView(ClientMixin, TableRequiredMixin, TemplateView):
	template_name = "clienttemplates/clientbasket.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		basket_id = self.request.session.get("mybasket", None)
		try:
			basket = Basket.objects.get(id = basket_id)
		except:
			basket = None
		context["mainbasket"] = basket

		return context




# Client AddToBasket View
class AddToBasketView(TemplateView):
	template_name = 'clienttemplates/clientaddtobasket.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)

		item_id = self.kwargs["pk"]
		item = Item.objects.get(id = item_id)

		basket_id = self.request.session.get("mybasket", None)
		if basket_id:
			mybasket = Basket.objects.get(id = basket_id)
			print("old basket &&&&&&&&&&&&&&&&&&&&&&&&")
		else:
			mybasket = Basket.objects.create(total = 0)
			self.request.session["mybasket"] = mybasket.id
			print("new basket &&&&&&&&&&&&&&&&&&&&&&&")


		basketitemquery = mybasket.basketitem_set.filter(item = item)
		if basketitemquery.exists():
			b_item = basketitemquery.first()
			b_item.quantity += 1
			b_item.subtotal += item.price
			b_item.save()
			mybasket.total += item.price
			mybasket.save()
			print("Item already in the basket *******************")

		else:
			b_item = BasketItem.objects.create(
				basket = mybasket,
				item = item,
				rate = item.price,
				quantity = 1,
				subtotal = item.price
				)
			mybasket.total += item.price
			mybasket.save()
			print("New Basket with new item created **************")



		return context





# Client Basket Manage View
class ManageBasketView(ClientMixin, TableRequiredMixin,  TemplateView):
	template_name = "clienttemplates/clientmanagebasket.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		basketitem_id = self.kwargs["pk"]
		action = self.kwargs["action"]
		# print(str(action) + "****************8")
		basketitem_obj = BasketItem.objects.get(id = basketitem_id)
		# print(basketitem_obj)
		basket = basketitem_obj.basket
		# print(basket)
		# orrrr
		# basket_id = self.request.session.get("mybasket", None)
		# basket = Basket.objects.get(id = basket_id)
		if action == 'inr':
			basketitem_obj.quantity += 1
			basketitem_obj.subtotal += basketitem_obj.rate
			basketitem_obj.save()
			basket.total += basketitem_obj.rate
			basket.save()
			context["message"] = "item '" + basketitem_obj.item.name + "' increased by 1"

		elif action == 'dcr':
			basketitem_obj.quantity -= 1
			basketitem_obj.subtotal -= basketitem_obj.rate
			basketitem_obj.save()
			basket.total -= basketitem_obj.rate
			basket.save()
			context["message"] = "item '" + basketitem_obj.item.name + "' decreased by 1"
		elif action == 'rmv':
			item_title = basketitem_obj.item.name
			basket.total -= basketitem_obj.subtotal
			basket.save()
			basketitem_obj.delete()
			context["message"] = "Item '" + item_title + "' removed Successfully..."
		else:
			context["message"] = "!!! Invalid Action !!!"

		context['main_basket'] = basket


		return context





# Client Empty Basket View
class EmptyBasketView(ClientMixin, TableRequiredMixin,  TemplateView):
	template_name = "clienttemplates/clientmanagebasket.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		bask_id = self.request.session.get("mybasket", None)

		if bask_id:
			basket_obj = Basket.objects.get(id = bask_id)
			basketitems = basket_obj.basketitem_set.all()
			basket_obj.total = 0
			basket_obj.save()
			basketitems.delete()
			context["message"] = "Your Basket Is Empty Now"
		else:
			context["message"] = "Order First To Empty Basket"

		return context





# Client Order Create View
class OrderCreateView(ClientMixin, TableRequiredMixin,  CreateView):
	template_name = 'clienttemplates/clientordercreate.html'
	form_class = OrderForm
	success_url = reverse_lazy("finalapp:clienthome")

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		basket_id = self.request.session.get("mybasket", None)
		basket = Basket.objects.get(id = basket_id)
		context["basket_obj"] = basket

		return context
	def form_valid(self, form):
		basket = Basket.objects.get(id = self.request.session.get("mybasket"))
		form.instance.basket = basket
		form.instance.table = basket.table
		print(str(form.instance.table) + "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
		form.instance.subtotal = basket.total
		form.instance.discount = 0
		form.instance.total = basket.total
		form.instance.order_status = "neworder"
		del self.request.session["mybasket"]

		return super().form_valid(form)





# Client Order List View
class OrderListView(ClientMixin, TableRequiredMixin, ListView):
	template_name = 'clienttemplates/clientorderlist.html'
	queryset = Order.objects.all()
	context_object_name = 'allorders'

	def get_queryset(self):
		data = super().get_queryset()
		table = Table.objects.get(user=self.request.user)
		return data.filter(table=table)


# Client Order Detail View
class OrderDetailView(ClientMixin, TableRequiredMixin, DetailView):
	template_name = 'clienttemplates/clientorderdetail.html'
	model = Order
	context_object_name = 'orderobject'









class ClientSearchView(TemplateView, TableRequiredMixin):
	template_name = "clienttemplates/clientsearch.html"

	def get_context_data(self,**kwargs):
		context = super().get_context_data(**kwargs)
		context['allmenus'] = Menu.objects.all()
		keyword = self.request.GET["alias"]
		#keyword = self.request.GET.get("dipak")
		# items = News.objects.filter(title__icontains = keyword)
		items = Item.objects.filter(
			Q(name__icontains = keyword) |
			Q(menu__title__icontains = keyword))
		context["keyword"] = keyword

		page = self.request.GET.get('page', 1)

		paginator = Paginator(items, 8)
		try:
		    results = paginator.page(page)
		except PageNotAnInteger:
		    results = paginator.page(1)
		except EmptyPage:
		    results = paginator.page(paginator.num_pages)
		#for pagination

		context["allitems"] = results



		print(items, "***********************")
		
		return context




class ClientPiHomeView(TemplateView):
	template_name = 'clienttemplates/clientpihome.html'

	



# $$$$$$$$$$$$$$&&&&&&&&&&&&&********************
# $$$$$$$$$$$$$$&&&&&&&&&&&&&********************
# $$$$$$$$$$$$$$&&&&&&&&&&&&&********************
# $$$$$$$$$$$$$$&&&&&&&&&&&&&********************
# $$$$$$$$$$$$$$&&&&&&&&&&&&&********************





# Admin AdminMixin View
class AdminMixin(object):
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context["restro"] = Restaurant.objects.all()
		context["alladmin"] = Admin.objects.filter()
		context["allmenus"] = Menu.objects.filter(root=None)
		context["allmenusnr"] = Menu.objects.all().order_by("-id")
		context["allitems"] = Item.objects.all().order_by("-id")
		# context["allspecialitems"] = SpecialItem.objects.all().order_by("-id")
		context["allorders"] = Order.objects.all().order_by("-id")
		context["neworders"] = Order.objects.filter(order_status='neworder').order_by("-id")
		context["preporders"] = Order.objects.filter(order_status='preparedorder').order_by("-id")
		context["allstaffs"] = Staff.objects.all().order_by("-id")
		# context["allservices"] = Service.objects.all().order_by("-id")
		# context["adminserviceform"] = AdminServiceForm
		context["aform"] = AdminSignupForm
		context["adminsigninform"] = AdminSigninForm
		context["orderform"] = OrderForm


		return context



# Admin AdminRequiredMixin View
class AdminRequiredMixin(object):
	def dispatch(self, request, *args, **kwargs):
		if request.user.is_authenticated and request.user.groups.filter(name="admin").exists():
			pass
		else:
			return redirect("finalapp:adminsignin")

		return super().dispatch(request, *args, **kwargs)




# Admin Signin View Class
class AdminSigninView(FormView):
	template_name = 'admintemplates/adminsignin.html'
	form_class = AdminSigninForm
	success_url = reverse_lazy("finalapp:adminhome")

	def form_valid(self, form):
		uname = form.cleaned_data['username']
		pwrd = form.cleaned_data['password']
		user = authenticate(username=uname, password=pwrd)
		if user is not None:
			login(self.request, user)
		else:
			return render(self.request, self.template_name,{
			'error': 'Invalid Username or Password!!!',
			'form': form
			})
		return super().form_valid(form)

	def get_success_url(self):
		logged_in_user = self.request.user
		if logged_in_user.groups.filter(name = 'admin').exists():
			return reverse("finalapp:adminhome")
		else:
			return reverse("finalapp:customersignup")



# Admin Signout view class
class AdminSignoutView(View):
	def get(self, request):
		logout(request)
		return redirect('/emenu/admin/home/')




class AdminHomeView(AdminMixin, AdminRequiredMixin, TemplateView):
	template_name = "admintemplates/adminhome.html"



# class AdminServiceListView(AdminMixin, AdminRequiredMixin, TemplateView):
# 	template_name = "admintemplates/adminservicelist.html"

# 	def get_context_data(self, **kwargs):
# 		context = super().get_context_data(**kwargs)
# 		service_obj = Service.objects.all().order_by("-id")

# 		page = self.request.GET.get('page',1)

# 		paginator = Paginator(service_obj, 4)
# 		try:
# 			results = paginator.page(page)
# 		except PageNotAnInteger:
# 			results = paginator.page(1)
# 		except EmptyPage:
# 			results = paginator.page(paginator.num_pages)

# 		context["allservices"] = results

# 		return context



# class AdminServiceDetailView(AdminMixin, AdminRequiredMixin, DetailView):
# 	template_name = "admintemplates/adminservicedetail.html"
# 	model = Service
# 	context_object_name = "serviceobject"


# class AdminServiceCreateView(AdminMixin, AdminRequiredMixin, CreateView):
# 	template_name = "admintemplates/adminservicecreate.html"
# 	form_class = AdminServiceForm
# 	success_url = "/emenu/admin/service/list/"


# class AdminServiceUpdateView(AdminMixin, AdminRequiredMixin, UpdateView):
# 	template_name = "admintemplates/adminserviceupdate.html"
# 	form_class = AdminServiceForm
# 	model = Service
# 	success_url = "/emenu/admin/service/list/"


# class AdminServiceDeleteView(AdminMixin, AdminRequiredMixin, DeleteView):
# 	template_name = "admintemplates/adminservicedelete.html"
# 	model = Service
# 	success_url = "/emenu/admin/service/list/"

# 	def get_context_data(self, **kwargs):
# 		context = super().get_context_data(**kwargs)
# 		service_id = self.kwargs["pk"]
# 		service = Service.objects.get(id=service_id)

# 		return context



class AdminMenuListView(AdminMixin, AdminRequiredMixin, TemplateView):
	template_name = "admintemplates/adminmenulist.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		menu_obj = Menu.objects.all().order_by("-id")

		page = self.request.GET.get('page',1)

		paginator = Paginator(menu_obj, 5)
		try:
			results = paginator.page(page)
		except PageNotAnInteger:
			results = paginator.page(1)
		except EmptyPage:
			results = paginator.page(paginator.num_pages)

		context["allmenusnr"] = results

		return context


class AdminMenuDetailView(AdminMixin, AdminRequiredMixin, DetailView):
	template_name = "admintemplates/adminmenudetail.html"
	model = Menu
	context_object_name = "menuobject"



class AdminMenuCreateView(AdminMixin, AdminRequiredMixin, CreateView):
	template_name = "admintemplates/adminmenucreate.html"
	form_class = AdminMenuForm
	success_url = "/emenu/admin/menu/list/"



class AdminMenuUpdateView(AdminMixin, AdminRequiredMixin, UpdateView):
	template_name = "admintemplates/adminmenuupdate.html"
	form_class = AdminMenuForm
	model = Menu
	success_url = "/emenu/admin/menu/list/"


class AdminMenuDeleteView(AdminMixin, AdminRequiredMixin, DeleteView):
	template_name = "admintemplates/adminmenudelete.html"
	model = Menu
	success_url = "/emenu/admin/menu/list/"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		menu_id = self.kwargs["pk"]
		menu = Menu.objects.get(id=menu_id)

		return context





class AdminItemListView(AdminMixin, AdminRequiredMixin, TemplateView):
	template_name = "admintemplates/adminitemlist.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		item_obj = Item.objects.all().order_by("-id")

		page = self.request.GET.get('page',1)

		paginator = Paginator(item_obj, 5)
		try:
			results = paginator.page(page)
		except PageNotAnInteger:
			results = paginator.page(1)
		except EmptyPage:
			results = paginator.page(paginator.num_pages)

		context["allitems"] = results

		return context





class AdminItemDetailView(AdminMixin, AdminRequiredMixin, DetailView):
	template_name = "admintemplates/adminitemdetail.html"
	model = Item
	context_object_name = "itemobject"



class AdminItemCreateView(AdminMixin, AdminRequiredMixin, CreateView):
	template_name = "admintemplates/adminitemcreate.html"
	form_class = AdminItemForm
	success_url = "/emenu/admin/item/list/"



class AdminItemUpdateView(AdminMixin, AdminRequiredMixin, UpdateView):
	template_name = "admintemplates/adminitemupdate.html"
	form_class = AdminItemForm
	model = Item
	success_url = "/emenu/admin/item/list/"


class AdminItemDeleteView(AdminMixin, AdminRequiredMixin, DeleteView):
	template_name = "admintemplates/adminitemdelete.html"
	model = Item
	success_url = "/emenu/admin/item/list/"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		item_id = self.kwargs["pk"]
		menu = Item.objects.get(id=item_id)

		return context





# class AdminSpecialItemListView(AdminMixin, AdminRequiredMixin, TemplateView):
# 	template_name = "admintemplates/adminspecialitemlist.html"

# 	def get_context_data(self, **kwargs):
# 		context = super().get_context_data(**kwargs)
# 		specialitem_obj = SpecialItem.objects.all().order_by("-id")

# 		page = self.request.GET.get('page',1)

# 		paginator = Paginator(specialitem_obj, 5)
# 		try:
# 			results = paginator.page(page)
# 		except PageNotAnInteger:
# 			results = paginator.page(1)
# 		except EmptyPage:
# 			results = paginator.page(paginator.num_pages)

# 		context["allspecialitems"] = results

# 		return context


# class AdminSpecialItemDetailView(AdminMixin, AdminRequiredMixin, DetailView):
# 	template_name = "admintemplates/adminspecialitemdetail.html"
# 	model = SpecialItem
# 	context_object_name = "specialitemobject"



# class AdminSpecialItemCreateView(AdminMixin, AdminRequiredMixin, CreateView):
# 	template_name = "admintemplates/adminspecialitemcreate.html"
# 	form_class = AdminSpecialItemForm
# 	success_url = "/emenu/admin/specialitem/list/"



# class AdminSpecialItemUpdateView(AdminMixin, AdminRequiredMixin, UpdateView):
# 	template_name = "admintemplates/adminspecialitemupdate.html"
# 	form_class = AdminSpecialItemForm
# 	model = SpecialItem
# 	success_url = "/emenu/admin/specialitem/list/"


# class AdminSpecialItemDeleteView(AdminMixin, AdminRequiredMixin, DeleteView):
# 	template_name = "admintemplates/adminspecialitemdelete.html"
# 	model = SpecialItem
# 	success_url = "/emenu/admin/specialitem/list/"

# 	def get_context_data(self, **kwargs):
# 		context = super().get_context_data(**kwargs)
# 		specialitem_id = self.kwargs["pk"]
# 		menu = SpecialItem.objects.get(id=specialitem_id)

# 		return context





class AdminBasketListView(AdminMixin, AdminRequiredMixin, TemplateView):
	template_name = "admintemplates/adminbasketlist.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		basket_obj = Basket.objects.all().order_by("-id")

		page = self.request.GET.get('page',1)

		paginator = Paginator(basket_obj, 5)
		try:
			results = paginator.page(page)
		except PageNotAnInteger:
			results = paginator.page(1)
		except EmptyPage:
			results = paginator.page(paginator.num_pages)

		context["allbaskets"] = results

		return context






class AdminOrderListView(AdminMixin, AdminRequiredMixin, TemplateView):
	template_name = "admintemplates/adminorderlist.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		if 'status' in self.request.GET:
			status = self.request.GET['status']
			order_obj = Order.objects.filter(order_status=status).order_by("-id")
		else:
			order_obj = Order.objects.all().order_by("-id")

		page = self.request.GET.get('page',1)

		paginator = Paginator(order_obj, 10)
		try:
			results = paginator.page(page)
		except PageNotAnInteger:
			results = paginator.page(1)
		except EmptyPage:
			results = paginator.page(paginator.num_pages)

		context["allorders"] = results

		return context



class AdminOrderUpdateView(AdminMixin, AdminRequiredMixin, UpdateView):
	template_name = "admintemplates/adminorderupdate.html"
	form_class = AdminOrderForm
	model = Order
	success_url = "/emenu/admin/order/list/"



class AdminOrderDetailView(AdminMixin, AdminRequiredMixin, DetailView):
	template_name = "admintemplates/adminorderdetail.html"
	model = Order
	context_object_name = "orderobject"



class AdminStaffListView(AdminMixin, AdminRequiredMixin, TemplateView):
	template_name = "admintemplates/adminstafflist.html"


# class AdminStaffSignupView(AdminMixin, AdminRequiredMixin, CreateView):
# 	template_name = "admintemplates/adminstaffsignup.html"
# 	form_class = AdminStaffForm
# 	success_url = "/emenu/admin/staff/list/"

# 	def form_valid(self, form):
# 		uname = form.cleaned_data["username"]
# 		email = form.cleaned_data["email"]
# 		password = form.cleaned_data["password"]
# 		user = User.objects.create_user(uname, email, password)
# 		form.instance.user = user

# 		return super().form_valid(form)



class AdminStaffCreateView(AdminMixin, AdminRequiredMixin, CreateView):
	template_name = "admintemplates/adminstaffcreate.html"
	form_class = AdminStaffForm
	success_url = "/emenu/admin/staff/list/"


class AdminStaffDetailView(AdminMixin, AdminRequiredMixin, DetailView):
	template_name = 'admintemplates/adminstaffdetail.html'
	model = Staff
	context_object_name = "staffobject"


class AdminStaffDeleteView(AdminMixin, AdminRequiredMixin, DeleteView):
	template_name = "admintemplates/adminstaffdelete.html"
	model = Staff
	success_url = "/emenu/admin/staff/list/"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		staff_id = self.kwargs["pk"]
		staff = Staff.objects.get(id=staff_id)

		return context




# AdminStaff Staff View Class
# class AdminStaffSigninView(FormView):
# 	template_name = 'admintemplates/adminstaffsignin.html'
# 	form_class = AdminStaffSigninForm
# 	success_url = reverse_lazy("finalapp:adminhome")

# 	def form_valid(self, form):
# 		uname = form.cleaned_data['username']
# 		pwrd = form.cleaned_data['password']
# 		user = authenticate(username=uname, password=pwrd)
# 		if user is not None:
# 			login(self.request, user)
# 		else:
# 			return render(self.request, self.template_name,{
# 			'error': 'Invalid Username or Password!!!',
# 			'form': form
# 			})
# 		return super().form_valid(form)

# 	def get_success_url(self):
# 		logged_in_user = self.request.user
# 		if logged_in_user.groups.filter(name = 'staff').exists():
# 			return reverse("finalapp:adminhome")
# 		else:
# 			return reverse("finalapp:adminstaffsignin")




class AdminSearchView(AdminMixin, AdminRequiredMixin, TemplateView):
	template_name = "admintemplates/adminsearch.html"

	def get_context_data(self,**kwargs):
		context = super().get_context_data(**kwargs)
		context['allmenus'] = Menu.objects.all()
		keyword = self.request.GET["alias"]
		#keyword = self.request.GET.get("dipak")
		# items = News.objects.filter(title__icontains = keyword)
		items = Item.objects.filter(
			Q(name__icontains = keyword) |
			Q(menu__title__icontains = keyword))
		context["keyword"] = keyword

		page = self.request.GET.get('page', 1)

		paginator = Paginator(items, 8)
		try:
		    results = paginator.page(page)
		except PageNotAnInteger:
		    results = paginator.page(1)
		except EmptyPage:
		    results = paginator.page(paginator.num_pages)
		#for pagination

		context["allitems"] = results



		print(items, "***********************")
		
		return context



class KitchenHomeView(AdminMixin, AdminRequiredMixin, TemplateView):
	template_name = "kitchentemplates/kitchenhome.html"


class KitchenOrderListView(AdminMixin, AdminRequiredMixin, TemplateView):
	template_name = "kitchentemplates/kitchenorderlist.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		if 'status' in self.request.GET:
			status = self.request.GET['status']
			order_obj = Order.objects.filter(order_status=status).order_by("-id")
		else:
			order_obj = Order.objects.all().order_by("-id")
		# order = Order.objects.all()
		page = self.request.GET.get('page',1)

		paginator = Paginator(order_obj, 10)
		try:
			results = paginator.page(page)
		except PageNotAnInteger:
			results = paginator.page(1)
		except EmptyPage:
			results = paginator.page(paginator.num_pages)

		context["allorders"] = results

		return context



class KitchenOrderDetailView(AdminMixin, AdminRequiredMixin, UpdateView):
	template_name = "kitchentemplates/kitchenorderdetail.html"
	form_class = AdminOrderForm
	model = Order
	success_url = "/emenu/kitchen/home/"




class TryView(TemplateView):
	template_name = 'admintemplates/admintry.html'
	