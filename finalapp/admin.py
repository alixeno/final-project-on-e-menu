from django.contrib import admin
from .models import *


admin.site.register([Restaurant, Admin, Table, Menu, Item, Basket, BasketItem, Order, Staff])