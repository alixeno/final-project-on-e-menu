from django import forms
from .models import *
from django.contrib.auth.models import User




class SigninForm(forms.Form):
	username = forms.CharField(widget = forms.TextInput(attrs={
		'class': 'form-control',
		'placeholder': 'Enter your username...'
		}))
	password = forms.CharField(widget=forms.PasswordInput(attrs={
		'class': 'form-control',
		'placeholder': 'Enter your password...'
		}))



#creation of class for form (signup)
class TableSignupForm(forms.ModelForm):
	username = forms.CharField(widget= forms.TextInput)
	email = forms.EmailField(widget = forms.EmailInput)
	password = forms.CharField(widget = forms.PasswordInput)
	confirm_password = forms.CharField(widget = forms.PasswordInput)
	class Meta:
		model = Table
		fields = ['username', 'email', 'password', 'confirm_password', 'name']
		#class vitra method lekhne for validation
		#first validation check for username
		def clean_username(self):
			uname = self.cleaned_data["username"]
			#cleaned_data le form ko euta field ko data line garxa
			#filter is used to query if such name exists in our database
			if User.objects.filter(username = uname).exists():
				raise forms.ValidationError("User with this username already exists")

			return uname

		#second validation check for email
		def clean_email(self):
			eemail = self.cleaned_data["email"]
			if User.objects.filter(email = eemail).exists():
				raise forms.ValidationError("Email with this email address already exists")
			return eemail

		def clean_confirm_password(self):
			password = self.cleaned_data["password"]
			c_pword = self.cleaned_data["confirm_password"]
			if password != c_pword:
				raise forms.ValidationError("Password didn't match")

			return c_pword



# Client Order Form class
class OrderForm(forms.ModelForm):
	class Meta:
		model = Order
		fields = ["payment_method"]







class AdminSigninForm(forms.Form):
	username = forms.CharField(widget = forms.TextInput(attrs={
		'class': 'form-control',
		'placeholder': 'Enter your username...'
		}))
	password = forms.CharField(widget=forms.PasswordInput(attrs={
		'class': 'form-control',
		'placeholder': 'Enter your password...'
		}))


#creation of class for form (signup)
class AdminSignupForm(forms.ModelForm):
	username = forms.CharField(widget= forms.TextInput)
	email = forms.EmailField(widget = forms.EmailInput)
	password = forms.CharField(widget = forms.PasswordInput)
	confirm_password = forms.CharField(widget = forms.PasswordInput)
	class Meta:
		model = Admin
		fields = ['username', 'email', 'password', 'confirm_password', 'name', 'mobile', 'address', 'photo']
		#class vitra method lekhne for validation
		#first validation check for username
		def clean_username(self):
			uname = self.cleaned_data["username"]
			#cleaned_data le form ko euta field ko data line garxa
			#filter is used to query if such name exists in our database
			if User.objects.filter(username = uname).exists():
				raise forms.ValidationError("User with this username already exists")

			return uname

		#second validation check for email
		def clean_email(self):
			eemail = self.cleaned_data["email"]
			if User.objects.filter(email = eemail).exists():
				raise forms.ValidationError("Email with this email address already exists")
			return eemail

		def clean_confirm_password(self):
			password = self.cleaned_data["password"]
			c_pword = self.cleaned_data["confirm_password"]
			if password != c_pword:
				raise forms.ValidationError("Password didn't match")

			return c_pword




# class AdminServiceForm(forms.ModelForm):
# 	class Meta:
# 		model = Service
# 		fields = "__all__"
# 		widgets = {
# 			'title': forms.TextInput(attrs={
# 				'class': 'form-control',
# 				'placeholder': 'Enter title...'
# 			}),
# 			'images': forms.ClearableFileInput(attrs={
# 				'class': 'form-control'
# 			}),
# 			'content': forms.Textarea(attrs={
# 				'class': 'form-control',
# 				'placeholder': 'Enter contents...'
# 			}),
# 		}



class AdminMenuForm(forms.ModelForm):
	class Meta:
		model = Menu
		fields = "__all__"
		widgets = {
		'title': forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter title'
			}),
		'caption': forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter caption...'
			}),

		}




class AdminItemForm(forms.ModelForm):
	class Meta:
		model = Item
		fields = "__all__"
		widgets = {
		'name': forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter item name'
			}),
		'sku': forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter sku for item...'
			}),
		'price': forms.TextInput(attrs={
			'class': 'form-control',
			'placeholder': 'Enter price for item...'
			})

		}





class AdminOrderForm(forms.ModelForm):
	class Meta:
		model = Order
		fields = ["order_status"]
		




# class AdminSpecialItemForm(forms.ModelForm):
# 	class Meta:
# 		model = SpecialItem
# 		fields = "__all__"
# 		widgets = {
# 		'name': forms.TextInput(attrs={
# 			'class': 'form-control',
# 			'placeholder': 'Enter item name'
# 			}),
# 		'sku': forms.TextInput(attrs={
# 			'class': 'form-control',
# 			'placeholder': 'Enter sku for item...'
# 			}),
# 		'price': forms.TextInput(attrs={
# 			'class': 'form-control',
# 			'placeholder': 'Enter price for item...'
# 			})

# 		}



class AdminStaffForm(forms.ModelForm):
	# username = forms.CharField(widget= forms.TextInput)
	# email = forms.EmailField(widget = forms.EmailInput)
	# password = forms.CharField(widget = forms.PasswordInput)
	# confirm_password = forms.CharField(widget = forms.PasswordInput)
	class Meta:
		model = Staff
		fields = ['name', 'address', 'mobile', 'position', 'photo']

		#class vitra method lekhne for validation
		#first validation check for username
		# def clean_username(self):
		# 	uname = self.cleaned_data["username"]
		# 	#cleaned_data le form ko euta field ko data line garxa
		# 	#filter is used to query if such name exists in our database
		# 	if User.objects.filter(username = uname).exists():
		# 		raise forms.ValidationError("User with this username already exists")

		# 	return uname

		# #second validation check for email
		# def clean_email(self):
		# 	eemail = self.cleaned_data["email"]
		# 	if User.objects.filter(email = eemail).exists():
		# 		raise forms.ValidationError("Email with this email address already exists")
		# 	return eemail

		# def clean_confirm_password(self):
		# 	password = self.cleaned_data["password"]
		# 	c_pword = self.cleaned_data["confirm_password"]
		# 	if password != c_pword:
		# 		raise forms.ValidationError("Password didn't match")

		# 	return c_pword




# class AdminStaffSigninForm(forms.Form):
# 	username = forms.CharField(widget = forms.TextInput(attrs={
# 		'class': 'form-control',
# 		'placeholder': 'Enter your username...'
# 		}))
# 	password = forms.CharField(widget=forms.PasswordInput(attrs={
# 		'class': 'form-control',
# 		'placeholder': 'Enter your password...'
# 		}))
